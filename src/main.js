import Router from './Router';
import data from './data';
import PizzaList from './pages/PizzaList';
import Component from './components/Component.js';
import PizzaForm from './pages/PizzaForm';

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');

const pizzaList = new PizzaList(data),
	aboutPage = new Component('p', null, 'ce site est génial'),
	pizzaForm = new PizzaForm();

Router.routes = [{ path: '/', page: pizzaList, title: 'La carte' }];

Router.navigate('/'); // affiche une page vide
Router.navigate('/'); // affiche la liste des pizzas

const titre = document.querySelector('.logo');
titre.innerHTML = titre.innerHTML + "<small>les pizzas c'est la vie</small>";

const newsContainer = document.querySelector(".newsContainer");
newsContainer.setAttribute('style', '');

function closeBtn(event){
    event.preventDefault();
    document.querySelector(".newsContainer").style.display = 'none';
}
const buttonClose = document.querySelector('.closeButton');
buttonClose.addEventListener('click', closeBtn);

Router.routes = [
	{ path: '/', page: pizzaList, title: 'La carte' },
	{ path: '/a-propos', page: aboutPage, title: 'À propos' },
	{ path: '/ajouter-pizza', page: pizzaForm, title: 'Ajouter une pizza' },
];

Router.menuElement = document.querySelector('.mainMenu');
