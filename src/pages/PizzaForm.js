import Page from './Page.js';

export default class PizzaForm extends Page {
	render() {
		return /*html*/ `
			<form class="pizzaForm">
				<label>
					Nom :
					<input type="text" name="name">
				</label>
				<button type="submit">Ajouter</button>
			</form>`;
	}

	mount(element) {
        const e = element.querySelector('button[type=submit]');
        e.addEventListener('click', this.submit);
		super.mount(element);
	}

	submit(event) {
        event.preventDefault();
        const name = event.currentTarget.parentNode.querySelector('input[name=name]')
        if(name.value == '') alert('Champ vide !');
        else{
            alert(`La pizza ${name.value} a été ajoutée`);
            name.value = '';
        }
    }
}
